﻿using UnityEngine;
using System.Collections;

public class LedScript : MonoBehaviour {

	public Sprite[] ledSprite;
	
	private bool isGreen;
	private bool change;
	
	void Start () {
		gameObject.GetComponent<SpriteRenderer>().sprite = ledSprite[0];
		change = false;
		isGreen = false;
	}
	
	void Update () {
		if (change) {
			change = false;
			if (!isGreen) {
				gameObject.GetComponent<SpriteRenderer>().sprite = ledSprite[1];
				isGreen = !isGreen;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().sprite = ledSprite[0];
				isGreen = !isGreen;
			}
		}
	}

	public void Change () {
		change = true;
	}
}
