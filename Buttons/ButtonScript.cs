﻿using UnityEngine;
using System.Collections;

public class ButtonScript : MonoBehaviour {

	public Sprite[] buttonSprite;
	private bool buttonDown;
	public bool pressed;

	void Start () {
		StopIt();
		pressed = false;
	}
	
	void OnMouseDown()
	{
		buttonDown = true;
		gameObject.GetComponent<SpriteRenderer>().sprite = buttonSprite[1];
	}
	
	void OnMouseUpAsButton()
	{
		if (buttonDown) {
			StopIt();			
			pressed = true;
		}
	}
	
	void OnMouseExit () {
		StopIt();
	}

	void StopIt() {
		buttonDown = false;
		gameObject.GetComponent<SpriteRenderer>().sprite = buttonSprite[0];
	}
}
