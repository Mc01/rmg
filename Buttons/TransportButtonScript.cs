﻿using UnityEngine;
using System.Collections;

public class TransportButtonScript : MonoBehaviour {
	
	public Sprite[] buttonSprite;
	private bool buttonDown;
	public bool pressed;
	
	void Start () {
		StopIt();
		pressed = false;
	}

	public void Click() {
		OnMouseDown();
	}

	void OnMouseDown()
	{
		if (buttonDown) {
			StopIt();
		}
		else {
			buttonDown = true;
			gameObject.GetComponent<SpriteRenderer>().sprite = buttonSprite[1];
		}
		pressed = true;
	}

	public void StopIt() {
		buttonDown = false;
		gameObject.GetComponent<SpriteRenderer>().sprite = buttonSprite[0];
	}
}
