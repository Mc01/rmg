﻿using UnityEngine;
using System.Collections;

public class PlaySound : MonoBehaviour {

	private AudioSource audioSound;

	void Awake () {
		audioSound = gameObject.GetComponent<AudioSource>();
	}

	public void Play() {
		audioSound.Play();
	}

	public void Pause() {
		audioSound.Pause();
	}

	public void Stop() {
		audioSound.Stop();
	}

	public void ChangePitch(float pitch) {
		audioSound.pitch = pitch;
	}

	public void ChangeVolume(float volume) {
		audioSound.volume = volume;
	}

	public void ChangePan(float pan) {
		audioSound.pan = pan;
	}
}
