﻿using UnityEngine;
using System.Collections;

public class Sequencer : MonoBehaviour {

	public MainModel model;
	public MainController controller;
	public Song song;

	public SequenceScript[] sequences;

	private int currentStep;
	private float playbackTime;
	private float timeInterval;

	private const int stepCount = 32;

	void Awake() {
		sequences = GetComponentsInChildren<SequenceScript>();
	}

	void Start() {
		StopSequencer();
		UpdateInterval();
	}

	public void PauseSequencer() {
		foreach (SequenceScript s in sequences) {
			s.PausePlayback();
		}
		song.GetPreviousClip().GetPos().StopIt();
		song.GetCurrentClip().GetPos().StopIt();
	}

	public void StopSequencer() {
		currentStep = 0;
		playbackTime = 0.0f;
		foreach (SequenceScript s in sequences) {
			s.StopPlayback();
		}
		song.GetPreviousClip().GetPos().StopIt();
		song.GetCurrentClip().GetPos().StopIt();
	}

	void UpdateInterval() {
		timeInterval = (60.0f / model.tempo) / 8.0f;
	}

	public void UpdateSequences() {
		UpdateInterval();
		if (model.isPlaying && playbackTime <= model.time) {
			if (model.state == MainModel.State.Song && currentStep == 0) {
				controller.ChangePattern(song.NextClipPattern());
			}
			foreach (SequenceScript s in sequences) {
				s.UpdateSequence(currentStep);
			}
			playbackTime += timeInterval;
			if (++currentStep >= stepCount) {
				if (model.state == MainModel.State.Song) {
					song.GetPreviousClip().GetPos().StopIt();
					song.GetCurrentClip().GetPos().StartIt();
				}
				currentStep = 0;
			}
		}
	}

	public int getStep() {
		return currentStep;
	}

	public SequenceScript[] GetSequences() {
		return sequences;
	}

	public void ClearSequencer() {
		foreach (SequenceScript s in sequences) {
			s.ClearSteps();
		}
	}
}
