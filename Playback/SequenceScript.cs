﻿using UnityEngine;
using System.Collections;

public class SequenceScript : MonoBehaviour {

	public PlaySound channel;

	public MainModel mainModel;

	public Step[] steps;
	public float pitch = 1.0f;
	public float volume = 0.8f;
	public float pan = 0;

	public string sampleName;
	public bool updateSampleName = false;

	void Awake() {
		mainModel = GameObject.Find("_Main Model").GetComponent<MainModel>();
		steps = new Step[transform.childCount];
		int i = 0;
		foreach (Transform step in transform) {
			steps[i++] = step.GetComponent<Step>();
		}
	}

	void Start() {
		pitch = 1.0f;
		volume = 0.8f;
		pan = 0;

		updateSampleName = false;
		if (channel.GetComponent<AudioSource>().clip!=null) {
			sampleName = channel.GetComponent<AudioSource>().clip.name;
		}
		else {
			sampleName = "empty";
		}
	}

	void Update() {
		if (updateSampleName) {
			if (channel.GetComponent<AudioSource>().clip!=null) {
				sampleName = channel.GetComponent<AudioSource>().clip.name;
			}
			else {
				sampleName = "empty";
			}
			updateSampleName = false;
		}
	}

	void OnGUI() {
		if (!mainModel.isShowingInstruments && mainModel.state == MainModel.State.Pattern) {
			Vector3 screenPos = Camera.main.WorldToScreenPoint(gameObject.transform.position);
			GUI.Label(new Rect(screenPos.x-0.293f*Screen.width, 0.955f*Screen.height-screenPos.y, 0.1f*Screen.width, 0.03f*Screen.height), sampleName);
//			if (channel.GetComponent<AudioSource>()==null)
//				Debug.Log(channel.GetComponent<AudioSource>().clip.name);

			GUI.Label(new Rect(screenPos.x-0.58f*Screen.width, 0.97f*Screen.height-screenPos.y, 0.1f*Screen.width, 0.03f*Screen.height), "Pitch");
			pitch = GUI.HorizontalSlider(new Rect(screenPos.x-0.58f*Screen.width, Screen.height-screenPos.y, 0.08f*Screen.width, 0.03f*Screen.height), pitch, 0.5f, 2.0f);
			GUI.Label(new Rect(screenPos.x-0.53f*Screen.width, 0.97f*Screen.height-screenPos.y, 0.1f*Screen.width, 0.03f*Screen.height), ( (float) pitch).ToString("0.00"));

			GUI.Label(new Rect(screenPos.x-0.48f*Screen.width, 0.97f*Screen.height-screenPos.y, 0.1f*Screen.width, 0.03f*Screen.height), "Pan");
			pan = GUI.HorizontalSlider(new Rect(screenPos.x-0.48f*Screen.width, Screen.height-screenPos.y, 0.08f*Screen.width, 0.03f*Screen.height), pan, -1.0f, 1.0f);
			GUI.Label(new Rect(screenPos.x-0.43f*Screen.width, 0.97f*Screen.height-screenPos.y, 0.1f*Screen.width, 0.03f*Screen.height), ( (float) pan).ToString("0.00"));

			GUI.Label(new Rect(screenPos.x-0.38f*Screen.width, 0.97f*Screen.height-screenPos.y, 0.1f*Screen.width, 0.03f*Screen.height), "Vol");
			volume = GUI.HorizontalSlider(new Rect(screenPos.x-0.38f*Screen.width, Screen.height-screenPos.y, 0.08f*Screen.width, 0.03f*Screen.height), volume, 0, 1.0f);
			GUI.Label(new Rect(screenPos.x-0.33f*Screen.width, 0.97f*Screen.height-screenPos.y, 0.1f*Screen.width, 0.03f*Screen.height), ( (float) volume).ToString("0.00"));
		}
	}

	public void UpdateSequence(int step) {
		if (steps[step].isActive) {
			channel.ChangePitch(pitch*steps[step].pitch);
			channel.ChangeVolume(volume*steps[step].volume);
			channel.ChangePan(pan);
			channel.Play();
		}
	}

	public void PausePlayback() {
		channel.Pause();
	}

	public void StopPlayback() {
		channel.Stop();
	}

	public void startTrackingFill() {
		foreach (Step s in steps) {
			s.isTrackingFill = true;
		}
	}

	public void stopTrackingFill() {
		foreach (Step s in steps) {
			s.isTrackingFill = false;
		}
	}

	public void startTrackingClean() {
		foreach (Step s in steps) {
			s.isTrackingClean = true;
		}
	}
	
	public void stopTrackingClean() {
		foreach (Step s in steps) {
			s.isTrackingClean = false;
		}
	}

	public Step[] getNotes() {
		return steps;
	}

	public void ClearSteps() {
		pitch = 1.0f;
		volume = 0.8f;
		pan = 0;
		foreach (Step step in steps) {
			step.Clear();
		}
	}
}
