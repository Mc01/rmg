﻿using UnityEngine;
using System.Collections;

public class PlaySoundController : MonoBehaviour {

	public KeyCode keyForSound;
	
	private PlaySound samplePlayer;

	void Awake () {
		samplePlayer = gameObject.GetComponent<PlaySound>();
	}

	void Update () {
		if (Input.GetKeyDown(keyForSound)) {
			samplePlayer.Play();
		}
	}
}
