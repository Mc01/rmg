﻿using UnityEngine;
using System.Collections;

public class Step : MonoBehaviour {

	public bool isActive;
	[Range(0.5f, 2.0f)] public float pitch = 1.0f;
	[Range(0.0f, 1.0f)] public float volume = 1.0f;

	public Sprite[] buttonSprite;

	public bool isTrackingFill;
	public bool isTrackingClean;
	private SequenceScript parent;
	private bool isCounting;
	private int frameCount;
	private bool isMod;
	private static MainModel model;
	private Vector3 initPos;

	void Awake() {
		parent = transform.parent.GetComponent<SequenceScript>();
	}

	void Start() {
		Clear();
		isTrackingFill = false;
		isTrackingClean = false;
		if (model == null) model = FindObjectOfType<MainModel>();
	}
	
	void OnMouseDown() {
		toggle();
		if (!model.guiLocked) {
			isCounting = true;
			frameCount = 0;
		}
		//RECENTLY CHANGED
		if (isActive) parent.startTrackingFill();
		else parent.startTrackingClean();
	}

	void OnMouseUp() {
		isCounting = false;
		if (isTrackingFill) parent.stopTrackingFill();
		else if (isTrackingClean) parent.stopTrackingClean();
	}

	void OnMouseOver() {
		if (isCounting) {
			frameCount++;
			if (frameCount > 15) {
				isCounting = false;
				isMod = true;
				model.guiLocked = true;
				initPos = new Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y, 0);
				if (initPos.y > Screen.height * 0.75f) initPos.y -= 0.25f * Screen.height;
				if (initPos.x > Screen.width *0.9f) initPos.x -= 0.1f * Screen.width;
				else if (initPos.x < Screen.width * 0.45f) initPos.x += 0.1f * Screen.width;
			}
		}
		else if (isTrackingFill && !isActive) {
			setOn();
		}
		else if (isTrackingClean && isActive) {
			setOff();
		}
	}

	void OnGUI() {
		if (isMod) {
			if (!isActive) setOn();
			GUI.Box(new Rect(initPos.x-0.1f*Screen.width,initPos.y,0.2f*Screen.width, 0.2f*Screen.height), "Quick Menu");

			GUI.Label(new Rect(initPos.x-0.08f*Screen.width, initPos.y+0.04f*Screen.height, 0.08f*Screen.width, 0.03f*Screen.height), "Pitch");
			pitch = GUI.HorizontalSlider(new Rect(initPos.x-0.07f*Screen.width, initPos.y+0.075f*Screen.height, 0.14f*Screen.width, 0.03f*Screen.height), pitch, 0.5f, 2.0f);
			GUI.Label(new Rect(initPos.x+0.005f*Screen.width, initPos.y+0.04f*Screen.height, 0.08f*Screen.width, 0.03f*Screen.height), ( (float) pitch).ToString("0.00"));

			GUI.Label(new Rect(initPos.x-0.08f*Screen.width, initPos.y+0.095f*Screen.height, 0.08f*Screen.width, 0.03f*Screen.height), "Vol");
			volume = GUI.HorizontalSlider(new Rect(initPos.x-0.07f*Screen.width, initPos.y+0.125f*Screen.height, 0.14f*Screen.width, 0.03f*Screen.height), volume, 0.0f, 1.0f);
			GUI.Label(new Rect(initPos.x+0.005f*Screen.width, initPos.y+0.095f*Screen.height, 0.08f*Screen.width, 0.03f*Screen.height), ( (float) volume).ToString("0.00"));

			if(GUI.Button(new Rect(initPos.x-0.07f*Screen.width, initPos.y+0.15f*Screen.height, 0.14f*Screen.width, 0.04f*Screen.height), "Close")) {
				isMod = false;
				model.guiLocked = false;
			}
		}
	}

	void OnMouseExit() {
		isCounting = false;
	}

	void toggle() {
		if (isActive) setOff();
		else setOn();
	}

	public void setOff() {
		gameObject.GetComponent<SpriteRenderer>().sprite = buttonSprite[0];
		isActive = false;
	}

	public void setOn() {
		gameObject.GetComponent<SpriteRenderer>().sprite = buttonSprite[1];
		isActive = true;
	}

	public void Clear() {
		setOff();
		pitch = 1.0f;
		volume = 1.0f;
	}

	public void LoadStep(bool isActive, float pitch, float volume) {
		if (isActive == true) {
			setOn();
		}
		this.pitch = pitch;
		this.volume = volume;
	}
}
