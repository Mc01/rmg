﻿using UnityEngine;
using System.Collections;

public class Song : MonoBehaviour {

	public Clip[] clips;

	private int currentClip;
	private float playbackTime;
	private float timeInterval;

	void Awake() {
		clips = GetComponentsInChildren<Clip>();
	}

	void Start() {
		StopSong();
	}

	public Clip GetPreviousClip() {
		if (currentClip-1 > -1)
			return clips[currentClip-1];
		else 
			return clips[0];
	}

	public Clip GetCurrentClip() {
		return clips[currentClip];
	}

	public int NextClipPattern() {
		return clips[currentClip++].clipPattern;
	}

	public void ClearSong() {
		foreach (Clip clip in clips) {
			clip.SetClipPattern(-1);
		}
	}

	public void StopSong() {
//		Debug.Log("Song.StopSong()");
		currentClip = 0;
	}
}
