﻿using UnityEngine;
using System;
using System.Collections;
//using System.IO;

public class SampleSelector : MonoBehaviour {

	public Sprite[] buttonSprite;
	private bool buttonDown;
	private bool pressed;

	public SequenceScript seq;

	public MainController mainController;

	void Awake () {
		mainController = GameObject.Find("_Main Controller").GetComponent<MainController>();
	}

	void Start () {
		StopIt();
		pressed = false;
	}
	
	void OnMouseDown()
	{
		buttonDown = true;
		gameObject.GetComponent<SpriteRenderer>().sprite = buttonSprite[1];
	}
	
	void OnMouseUpAsButton()
	{
		if (buttonDown && !mainController.mainModel.guiLocked) {
			StopIt();			
			pressed = true;
			mainController.ShowInstruments(seq.channel);
		}
	}
	
	void OnMouseExit ()
	{
		StopIt();
	}
	
	void StopIt()
	{
		buttonDown = false;
		gameObject.GetComponent<SpriteRenderer>().sprite = buttonSprite[0];
	}
}
