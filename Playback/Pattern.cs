﻿using UnityEngine;
using System.Collections;

public class Pattern {

	public class Sequence { 
		public PatternStep[] steps;
		public float pitch;
		public float volume;
		public float pan;
	}

	public class PatternStep { 
		public bool isActive;
		public float pitch;
		public float volume;
	} 

	private Sequencer sequencer;
	public Sequence[] sequences;

	public void SavePattern() {
		if (sequencer == null)
			sequencer = GameObject.Find("Step Sequencer").GetComponent<Sequencer>();

		if (sequences == null)
			sequences = new Sequence[16];

		for (int i = 0; i < sequences.Length; i++) {
			if (sequences[i] == null)
				sequences[i] = new Sequence();

			sequences[i].pitch = sequencer.sequences[i].pitch;
			sequences[i].volume = sequencer.sequences[i].volume;
			sequences[i].pan = sequencer.sequences[i].pan;

			sequences[i].steps = new PatternStep[32];
			for (int j = 0; j < sequences[i].steps.Length; j++) {
				if (sequences[i].steps[j] == null)
					sequences[i].steps[j] = new PatternStep();

				sequences[i].steps[j].isActive = sequencer.sequences[i].steps[j].isActive;
				sequences[i].steps[j].pitch = sequencer.sequences[i].steps[j].pitch;
				sequences[i].steps[j].volume = sequencer.sequences[i].steps[j].volume;
			}
		}
	}

	public void LoadPattern() {
		if (sequencer == null)
			sequencer = GameObject.Find("Step Sequencer").GetComponent<Sequencer>();
		
		if (sequences != null) {
			for (int i = 0; i < sequences.Length; i++) {
				sequencer.sequences[i].pitch = sequences[i].pitch;
				sequencer.sequences[i].volume = sequences[i].volume;
				sequencer.sequences[i].pan = sequences[i].pan;

				for (int j = 0; j < sequencer.sequences[i].steps.Length; j++) {
					sequencer.sequences[i].steps[j].LoadStep(sequences[i].steps[j].isActive, sequences[i].steps[j].pitch, sequences[i].steps[j].volume);
					if (sequences[i].steps[j].isActive) {
					}
				}
			}

		}
	}
}
