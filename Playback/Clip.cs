﻿using UnityEngine;
using System.Collections;

public class Clip : MonoBehaviour {
	public int clipPattern;
	private MainModel model;
	public bool isActive;
	Vector3 thisSize;
	GUIStyle style;
	Vector3 thisPos;
	private Pos position;

	void Start() {
		clipPattern = -1;
		model = FindObjectOfType<MainModel>();
		style = new GUIStyle();
		style.normal.textColor = Color.black;
		position = GetComponentInChildren<Pos>();
	}

	void OnMouseDown() {
		if (clipPattern != -1) SetClipPattern(-1);
		else SetClipPattern(model.currentSongPattern);
	}

	public void SetClipPattern(int pattern) {
		clipPattern = pattern;
		if (pattern != -1) {
			thisPos = gameObject.transform.position;
			thisSize = this.GetComponent<BoxCollider2D>().size;
			isActive = true;
		}
		else isActive = false;
	}

	void OnGUI() {
		if (isActive) {
			if (thisPos != gameObject.transform.position) {
				thisPos = gameObject.transform.position;
				thisSize = this.GetComponent<BoxCollider2D>().size;
			}
			thisPos = Camera.main.WorldToScreenPoint(new Vector3(thisPos.x - thisSize.x / 9, thisPos.y + thisSize.y / 3, thisPos.z));
			thisPos.y = Screen.height - thisPos.y;
			GUI.Label(new Rect(thisPos.x, thisPos.y, thisSize.x * 2/3, thisSize.y * 2/3), (clipPattern + 1).ToString(), style);
		}
	}

	public Pos GetPos() {
		return position;
	}
}
