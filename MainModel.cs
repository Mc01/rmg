﻿using UnityEngine;
using System.Collections;

public class MainModel : MonoBehaviour {

	public enum State {
		Pattern,
		Song,
		Options
	}

	public string[] fileNames = new string[16];
	public float tempo = 140;
	public Pattern[] patterns;
	public int currentPattern = 0;
	public int previousCurrentPattern = 0;
	public int currentSongPattern = 0;

	public bool guiLocked = false;

	public float time = 0.0f;
	public bool isPlaying = false;
	public bool isShowingInstruments = false;

	private string copyPath = "copyFile.xml";
	public string savePath = "saveFile.xml";

	public State state = State.Pattern;

	void Start() {
		patterns = new Pattern[33];
		for (int i = 0; i < patterns.Length; i++) {
			patterns[i] = new Pattern();
		}

		currentPattern = 0;
		tempo = 80;
	}

	public void UpdateModel() {
		if (isPlaying) {
			time += Time.deltaTime;
		}
	}

	public string getCopyPath() {
		return copyPath;
	}
}
