using UnityEngine;
using System;
using System.Collections;
using System.IO;

public class MainController : MonoBehaviour {

	private Loader loader;

	public MainModel mainModel;
	public Positioner positioner;
	public Sequencer sequencer;
	public Song song;

	public GameObject transport;
	public TransportButtonScript playButton;
	public ButtonScript stopButton;
	public LedScript led;

	public ButtonScript patternButton;
	public ButtonScript songButton;
	public ButtonScript optionsButton;

	private FileInfo[] kicks;
	private FileInfo[] snares;
	private FileInfo[] openHats;
	private FileInfo[] closedHats;
	private FileInfo[] cymbals;
	private FileInfo[] claps;
	private FileInfo[] percs;
	private FileInfo[] basses;
	private FileInfo[] instruments;
	private FileInfo[] fx;

	private string absolutePath = "./";
	private AudioSource currentChannel;

	#region MonoBehaviour methods

	void Awake() {
		loader = new Loader();
		loader.Synchronize(mainModel.getCopyPath(), mainModel.savePath);
	}

	void Start() {
		if(Application.isEditor) absolutePath = "Assets/";
		LoadFileInfo();
	}

	void Update() {
		mainModel.UpdateModel();
		if (mainModel.isPlaying) {
			sequencer.UpdateSequences();
			if (mainModel.state == MainModel.State.Pattern) {
				positioner.ChangePos(sequencer.getStep());
			}
			else if (mainModel.state == MainModel.State.Song) {

			}
			else {
				Stop();
			}
		}
		UpdateInput();
	}

	void OnGUI() {
		if (!mainModel.isShowingInstruments && mainModel.state != MainModel.State.Options) {
			GUI.Label(new Rect(0.024f*Screen.width, 0.005f*Screen.height, 0.1f*Screen.width, 0.05f*Screen.height), "Tempo");
			mainModel.tempo = GUI.HorizontalSlider(new Rect(0.024f*Screen.width, 0.035f*Screen.height, 0.1f*Screen.width, 0.03f*Screen.height), mainModel.tempo, 50.0f, 200.0f);
			GUI.Label(new Rect(0.084f*Screen.width, 0.005f*Screen.height, 0.1f*Screen.width, 0.05f*Screen.height), ( (float) mainModel.tempo).ToString("0.00"));

			if (mainModel.state != MainModel.State.Options) {
				if (GUI.Button(new Rect(0.3f*Screen.width, 0.011f*Screen.height, 0.05f*Screen.width, 0.04f*Screen.height), "Clear")) {
					if (mainModel.state == MainModel.State.Pattern) {
						Clear();
					}
					else {
						song.ClearSong();
					}
				}
			}

			GUI.Label(new Rect(0.16f*Screen.width, 0.005f*Screen.height, 0.1f*Screen.width, 0.05f*Screen.height), "Pattern:");
			GUI.Label(new Rect(0.16f*Screen.width, 0.025f*Screen.height, 0.1f*Screen.width, 0.05f*Screen.height), (mainModel.state == MainModel.State.Pattern) ? ( (int) mainModel.currentPattern+1).ToString() : ( (int) mainModel.currentSongPattern+1).ToString());
			if (GUI.Button(new Rect(0.22f*Screen.width, 0.011f*Screen.height, 0.03f*Screen.width, 0.04f*Screen.height), "-"))
				DecreasePattern();
			if (GUI.Button(new Rect(0.25f*Screen.width, 0.011f*Screen.height, 0.03f*Screen.width, 0.04f*Screen.height), "+"))
				IncreasePattern();
		}
		else if (mainModel.state == MainModel.State.Pattern) {
			ShowInstrumentButtons(kicks, "Kicks", 0);
			ShowInstrumentButtons(snares, "Snares", 0.1f);
			ShowInstrumentButtons(openHats, "OpenHats", 0.2f);
			ShowInstrumentButtons(closedHats, "ClosedHats", 0.3f);
			ShowInstrumentButtons(cymbals, "Cymbals", 0.4f);
			ShowInstrumentButtons(claps, "Claps", 0.5f);
			ShowInstrumentButtons(percs, "Percs", 0.6f);
			ShowInstrumentButtons(basses, "Basses", 0.7f);
			ShowInstrumentButtons(instruments, "Instruments", 0.8f);
			ShowInstrumentButtons(fx, "FX", 0.9f);
		}
		else if (mainModel.state == MainModel.State.Options) {
			GUI.Label(new Rect(0.1f*Screen.width, 0.1f*Screen.height, 0.1f*Screen.width, 0.05f*Screen.height), "Save File:");
			mainModel.savePath = GUI.TextField(new Rect(0.2f*Screen.width, 0.1f*Screen.height, 0.15f*Screen.width, 0.05f*Screen.height), mainModel.savePath, 25);
		}
	}

	#endregion

	#region Helper Methods

	#region Buttons

	private void UpdateInput() {
		if (Input.GetKeyUp(KeyCode.Space)) {
			if (!mainModel.isPlaying) playButton.Click();
			else {
				if (Input.GetKey(KeyCode.LeftControl)) {
					playButton.Click();
				}
				else {
					stopButton.pressed = true;
				}
			}
		}
		else if (Input.GetKeyUp(KeyCode.C) && Input.GetKey(KeyCode.LeftShift)) {
			Pattern pattern = mainModel.patterns[mainModel.currentPattern];
			pattern.SavePattern();
			loader.Copy(pattern);
		}
		else if (Input.GetKeyUp(KeyCode.V) && Input.GetKey(KeyCode.LeftShift)) {
			Clear();
			Pattern pattern = mainModel.patterns[mainModel.currentPattern];
			pattern = loader.Paste();
			pattern.LoadPattern();
		}
		else if (Input.GetKeyUp(KeyCode.S) && Input.GetKey(KeyCode.LeftShift)) {
			loader.Synchronize(mainModel.getCopyPath(), mainModel.savePath);
			mainModel.patterns[mainModel.currentPattern].SavePattern();
			loader.Save(mainModel, song);
		}
		else if (Input.GetKeyUp(KeyCode.O) && Input.GetKey(KeyCode.LeftShift)) {
			Clear();
			loader.Synchronize(mainModel.getCopyPath(), mainModel.savePath);
			loader.Load(mainModel, song);
			loadInstruments(mainModel.fileNames);
			mainModel.patterns[mainModel.currentPattern].LoadPattern();
		}
		else if (Input.GetKeyUp(KeyCode.Equals)) {
			IncreasePattern();
		}
		else if (Input.GetKeyUp(KeyCode.Minus)) {
			DecreasePattern();
		}

		if (playButton.pressed && !mainModel.isPlaying) {
			playButton.pressed = false;
			Play();
		}
		else if (playButton.pressed && mainModel.isPlaying) {
			playButton.pressed = false;
			Pause();
		}
		else if (stopButton.pressed) {
			stopButton.pressed = false;
			Stop();
		}
		else if (patternButton.pressed) {
			patternButton.pressed = false;
			showPattern();
		}
		else if (songButton.pressed) {
			songButton.pressed = false;
			showSong();
		}
		else if (optionsButton.pressed) {
			optionsButton.pressed = false;
			showOptions();
		}
	}

	private void Play() {
		if (!mainModel.isPlaying) {
			mainModel.isPlaying = true;
			led.Change();
			song.GetCurrentClip().GetPos().StartIt();
		}
	}

	private void Pause() {
		if (mainModel.isPlaying) {
			mainModel.isPlaying = false;
			playButton.StopIt();
			led.Change();
			sequencer.PauseSequencer();
		}
	}

	private void Stop() {
		if (mainModel.isPlaying) {
			Pause();
		}
		mainModel.time = 0.0f;
		sequencer.StopSequencer();
		song.StopSong();
		positioner.ChangePos(sequencer.getStep());
	}

	private void Clear() {
		sequencer.ClearSequencer();
	}

	private void IncreasePattern() {
		if (mainModel.state == MainModel.State.Pattern && mainModel.currentPattern < 31) {
			mainModel.patterns[mainModel.currentPattern].SavePattern();
			Clear();
			++mainModel.currentPattern;
			mainModel.patterns[mainModel.currentPattern].LoadPattern();
		}
		else if (mainModel.state == MainModel.State.Song && mainModel.currentSongPattern < 31) {
			++mainModel.currentSongPattern;
		}
	}

	private void DecreasePattern() {
		if (mainModel.state == MainModel.State.Pattern && mainModel.currentPattern > 0) {
			mainModel.patterns[mainModel.currentPattern].SavePattern();
			Clear();
			--mainModel.currentPattern;
			mainModel.patterns[mainModel.currentPattern].LoadPattern();
		}
		else if (mainModel.state == MainModel.State.Song && mainModel.currentSongPattern > 0) {
			--mainModel.currentSongPattern;
		}
	}

	public void ChangePattern(int pat) {
		if (pat == -1) pat = 32;
		if (pat >= 0 && pat <= 32) {
			mainModel.patterns[mainModel.currentPattern].SavePattern();
			Clear();
			mainModel.currentPattern = pat;
			mainModel.patterns[mainModel.currentPattern].LoadPattern();
		}
	}

	private void showPattern() {
		if (mainModel.state != MainModel.State.Pattern) {
			Stop();
//			mainModel.patterns[mainModel.currentPattern].LoadPattern();
			ChangePattern(mainModel.previousCurrentPattern);
			transport.transform.position = new Vector3(-0.155f, transport.transform.position.y, 0);
			sequencer.transform.position = new Vector3(0.75f, sequencer.transform.position.y, 0);
			song.transform.position = new Vector3(8.0f, song.transform.position.y, 0);
			mainModel.state = MainModel.State.Pattern;
		}
	}

	private void showSong() {
		if (mainModel.state != MainModel.State.Song) {
			Stop();
//			mainModel.patterns[mainModel.currentPattern].SavePattern();
//			Clear();
			mainModel.previousCurrentPattern = mainModel.currentPattern;
			transport.transform.position = new Vector3(-0.155f, transport.transform.position.y, 0);
			sequencer.transform.position = new Vector3(7.0f, sequencer.transform.position.y, 0);
			song.transform.position = new Vector3(0, song.transform.position.y, 0);
			mainModel.state = MainModel.State.Song;
		}
	}

	private void showOptions() {
		if (mainModel.state != MainModel.State.Options) {
			Stop();
//			mainModel.patterns[mainModel.currentPattern].SavePattern();
//			Clear();
			mainModel.previousCurrentPattern = mainModel.currentPattern;
			transport.transform.position = new Vector3(5.0f, transport.transform.position.y, 0);
			sequencer.transform.position = new Vector3(7.0f, sequencer.transform.position.y, 0);
			song.transform.position = new Vector3(8.0f, song.transform.position.y, 0);
			mainModel.state = MainModel.State.Options;
		}
	}

	#endregion

	public void ShowInstruments(PlaySound channel) {
		currentChannel = channel.GetComponent<AudioSource>();
		mainModel.isShowingInstruments = true;
		Camera.main.transform.position = new Vector3(-10.0f, 0, -10.0f);
	}

	private void HideInstruments() {
		mainModel.isShowingInstruments = false;
		Camera.main.transform.position = new Vector3(0, 0, -10.0f);
	}

	private void loadInstruments (string[] fileNames)
	{
		int size = fileNames.Length;
		PlaySound[] channels = FindObjectsOfType<PlaySound>() as PlaySound[];
		for (int i = 0; i < size; i++) {
			if (fileNames[i] != "")	{
				foreach (PlaySound channel in channels) {
					AudioSource current = channel.GetComponent<AudioSource>();
					if (int.Parse(current.gameObject.name) == i) {
						current.clip = (AudioClip) Resources.Load(fileNames[i]);
					}	
				}
			}
		}

		foreach (SequenceScript seq in sequencer.sequences) {
			seq.updateSampleName = true;
		}
	}

	private void LoadFileInfo()
	{
		kicks = new DirectoryInfo (absolutePath + "Resources/Kicks").GetFiles ("*.wav");
		SortFileInfo (kicks);
		snares = new DirectoryInfo (absolutePath + "Resources/Snares").GetFiles ("*.wav");
		SortFileInfo (snares);
		openHats = new DirectoryInfo (absolutePath + "Resources/OpenHats").GetFiles ("*.wav");
		SortFileInfo (openHats);
		closedHats = new DirectoryInfo (absolutePath + "Resources/ClosedHats").GetFiles ("*.wav");
		SortFileInfo (closedHats);
		cymbals = new DirectoryInfo (absolutePath + "Resources/Cymbals").GetFiles ("*.wav");
		SortFileInfo (cymbals);
		claps = new DirectoryInfo (absolutePath + "Resources/Claps").GetFiles ("*.wav");
		SortFileInfo (claps);
		percs = new DirectoryInfo (absolutePath + "Resources/Percs").GetFiles ("*.wav");
		SortFileInfo (percs);
		basses = new DirectoryInfo (absolutePath + "Resources/Basses").GetFiles ("*.wav");
		SortFileInfo (basses);
		instruments = new DirectoryInfo (absolutePath + "Resources/Instruments").GetFiles ("*.wav");
		SortFileInfo (instruments);
		fx = new DirectoryInfo (absolutePath + "Resources/FX").GetFiles ("*.wav");
		SortFileInfo (fx);
	}
	
	private void SortFileInfo(FileInfo[] instruments) {
		Array.Sort(instruments, delegate (FileInfo x, FileInfo y) {
			return string.Compare(x.Name, y.Name);
		});
	}

	private void ShowInstrumentButtons(FileInfo[] fileInfo, string name, float position) {
		GUI.Label(new Rect(position*Screen.width, 0.003f*Screen.height, 0.1f*Screen.width, 0.05f*Screen.height), name);

		float shift = 0.03f;
		foreach (FileInfo f in fileInfo) 
		{
            if (GUI.Button(new Rect(position*Screen.width, shift*Screen.height, (Screen.width/10)-0.001f, 0.035f*Screen.height), f.Name.Replace(".wav", ""))) {
				string fileName = f.Name;
				fileName = fileName.Replace(".wav", "");
				fileName = name + "/" + fileName;
//				Debug.Log("File name: " + fileName);
				mainModel.fileNames[int.Parse(currentChannel.gameObject.name)] = fileName;

				currentChannel.clip = (AudioClip) Resources.Load(fileName);
				foreach (SequenceScript seq in sequencer.sequences) {
					seq.updateSampleName = true;
				}
				HideInstruments();
			}
			shift += 0.04f;
		}
	}

	#endregion

}
