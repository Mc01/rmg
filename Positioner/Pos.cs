﻿using UnityEngine;
using System.Collections;

public class Pos : MonoBehaviour {

	public Sprite[] buttonSprite;
	public int position = -1;

	void Start () {
		StopIt();
	}

	public void StartIt() {
		gameObject.GetComponent<SpriteRenderer>().sprite = buttonSprite[1];
	}

	public void StopIt() {
		gameObject.GetComponent<SpriteRenderer>().sprite = buttonSprite[0];
	}
}
