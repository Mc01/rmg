﻿using UnityEngine;
using System.Collections;

public class Positioner : MonoBehaviour {

	private Pos[] positions;
	private int prevStep = -1;
	
	void Start () {
		positions = new Pos[32];
		Pos[] unsortedPositions = this.GetComponentsInChildren<Pos>();
		foreach (Pos pos in unsortedPositions) {
			positions[pos.position] = pos;
		}
	}

	public void ChangePos(int step) {
		if (prevStep != -1 && prevStep != step) {
			positions[prevStep].StopIt();
		}
		positions[step].StartIt();
		prevStep = step;
	}
}
