﻿using System.Xml;
using System.Xml.Serialization;

public class SerializedChannel {
	[XmlElement("Pitch")]
	public float pitch;
	[XmlElement("Pan")]
	public float pan;
	[XmlElement("Volume")]
	public float volume;
	[XmlArray("Steps"), XmlArrayItem("Step")]
	public SerializedStep[] steps;
}