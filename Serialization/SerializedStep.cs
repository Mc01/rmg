﻿using System.Xml;
using System.Xml.Serialization;

public class SerializedStep {
	[XmlElement("Pitch")]
	public float pitch;
	[XmlElement("Volume")]
	public float volume;
	[XmlElement("Active")]
	public bool isActive;
}