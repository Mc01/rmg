﻿using System.Xml;
using System.Xml.Serialization;

[XmlRoot("ClipboardPattern")]
public class ClipboardPattern {
	[XmlArray("Channels"), XmlArrayItem("Channel")]
	public SerializedChannel[] channels;
}