﻿using System.Xml;
using System.Xml.Serialization;

[XmlRoot("SerializedObject")]
public class SerializedProject {
	[XmlElement("Tempo")]
	public float tempo;
	[XmlArray("Channels"), XmlArrayItem("Channel")]
	public string[] channels;
	[XmlArray("Patterns"), XmlArrayItem("Pattern")]
	public SerializedPattern[] patterns;
	[XmlArray("Song"), XmlArrayItem("Clip")]
	public int[] clips;
}
