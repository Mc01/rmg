﻿using System.Xml;
using System.Xml.Serialization;

public class SerializedPattern {
	[XmlArray("Channels"), XmlArrayItem("Channel")]
	public SerializedChannel[] channels;
}
