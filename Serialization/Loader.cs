﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class Loader {
	private string copyPath;
	private string savePath;
	private ClipboardPattern copyObject;
	private SerializedProject saveObject;

	public void Synchronize(string copyPath ,string savePath) {
		this.copyPath = copyPath;
		this.savePath = savePath;
	}

	public bool Copy(Pattern pattern) {
		Debug.Log("Copy start.");
		int size = pattern.sequences.Length;
		if (size > 0) {
			copyObject = new ClipboardPattern();
			copyObject.channels = serializePattern(pattern, size);
			copyXML(copyObject, copyPath);
			Debug.Log("Copy success.");
			return true;
		}
		else {
			Debug.Log("Copy fail.");
			return false;
		}
	}

	SerializedChannel[] serializePattern(Pattern pattern, int size) {
		SerializedChannel[] channels = new SerializedChannel[size];
		for (int i = 0; i < size; i++) {
			SerializedChannel serializedChannel = new SerializedChannel();
			serializedChannel.pan = pattern.sequences[i].pan;
			serializedChannel.pitch = pattern.sequences[i].pitch;
			serializedChannel.volume = pattern.sequences[i].volume;
			Pattern.PatternStep[] steps = pattern.sequences[i].steps;
			int length = steps.Length;
			serializedChannel.steps = new SerializedStep[length];
			for (int j = 0; j < length; j++) {
				SerializedStep serializedStep = new SerializedStep();
				serializedStep.pitch = steps[j].pitch;
				serializedStep.volume = steps[j].volume;
				serializedStep.isActive = steps[j].isActive;
				serializedChannel.steps[j] = serializedStep;
			}
			channels[i] = serializedChannel;
		}
		return channels;
	}

	void copyXML (ClipboardPattern serial, string path) {
		XmlSerializer serializer = new XmlSerializer(typeof(ClipboardPattern));
		FileStream stream  = new FileStream(path, FileMode.Create);
		serializer.Serialize(stream, serial);
		stream.Close();
	}

	public Pattern Paste() {
		Debug.Log("Paste start.");
		copyObject = pasteXML(copyPath);
		int size = copyObject.channels.Length;
		if (size > 0) {
			Debug.Log("Paste success.");
			return deserializePattern(copyObject.channels, size);
		}
		else {
			Debug.Log("Paste fail.");
			return null;
		}
	}

	ClipboardPattern pasteXML(string path) {
		XmlSerializer serializer = new XmlSerializer(typeof(ClipboardPattern));
		FileStream stream = new FileStream(path, FileMode.Open);
		ClipboardPattern serial = serializer.Deserialize(stream) as ClipboardPattern;
		stream.Close();
		return serial;
	}

	Pattern deserializePattern(SerializedChannel[] channels, int size) {
		Pattern pattern = new Pattern();
		pattern.sequences = new Pattern.Sequence[size];
		for (int i = 0; i < size; i++) {
			Pattern.Sequence sequence = new Pattern.Sequence();
			sequence.pan = channels[i].pan;
			sequence.pitch = channels[i].pitch;
			sequence.volume = channels[i].volume;
			SerializedStep[] serializedSteps = channels[i].steps;
			int length = serializedSteps.Length;
			sequence.steps = new Pattern.PatternStep[length];
			for (int j = 0; j < length; j++) {
				Pattern.PatternStep step = new Pattern.PatternStep();
				step.pitch = serializedSteps[j].pitch;
				step.volume = serializedSteps[j].volume;
				step.isActive = serializedSteps[j].isActive;
				sequence.steps[j] = step;
			}
			pattern.sequences[i] = sequence;
		}
		return pattern;
	}

	public bool Save(MainModel model, Song song) {
		Debug.Log("Save start.");
		int size = model.patterns.Length;
		if (size > 0) {
			saveObject = new SerializedProject();
			saveObject.tempo = model.tempo;
			saveObject.channels = model.fileNames;
			saveObject.patterns = new SerializedPattern[size];
			for (int i = 0; i < size; i++) {
				SerializedPattern pattern = new SerializedPattern();
				if (model.patterns[i].sequences != null) {
					int length = model.patterns[i].sequences.Length;
					pattern.channels = serializePattern(model.patterns[i], length);
				}
				saveObject.patterns[i] = pattern;
			}
			saveObject.clips = new int[song.clips.Length];
			int iterate = 0;
			foreach(Clip clip in song.clips) {
				saveObject.clips[iterate] = clip.clipPattern;
				iterate++;
			}
			saveXML(saveObject, savePath);
			Debug.Log("Save success.");
			return true;
		}
		else {
			Debug.Log("Save fail.");
			return false;
		}
	}

	void saveXML (SerializedProject serial, string path) {
		XmlSerializer serializer = new XmlSerializer(typeof(SerializedProject));
		FileStream stream  = new FileStream(path, FileMode.Create);
		serializer.Serialize(stream, serial);
		stream.Close();
	}

	public bool Load(MainModel model, Song song) {
		if (System.IO.File.Exists(savePath))
		{
			Debug.Log("Load start.");
			saveObject = loadXML(savePath);
			int size = saveObject.patterns.Length;
			if (size > 0) {
				model.tempo = saveObject.tempo;
				model.fileNames = saveObject.channels;
				for (int i = 0; i < size; i++) {
					SerializedPattern serializedPattern = saveObject.patterns[i];
					Pattern pattern = new Pattern();
					if (serializedPattern.channels != null) {
						pattern = deserializePattern(serializedPattern.channels, serializedPattern.channels.Length);
					}
					model.patterns[i] = pattern;
				}
				int iterate = 0;
				foreach (int saveClip in saveObject.clips) {
					song.clips[iterate].clipPattern = saveClip;
					if (saveClip != -1) song.clips[iterate].isActive = true;
					else song.clips[iterate].isActive = false;
					iterate++;
				}
				Debug.Log("Load success.");
				return true;
			}
		}
		Debug.Log("Load fail.");
		return false;
	}

	SerializedProject loadXML(string path) {
		XmlSerializer serializer = new XmlSerializer(typeof(SerializedProject));
		FileStream stream = new FileStream(path, FileMode.Open);
		SerializedProject serial = serializer.Deserialize(stream) as SerializedProject;
		stream.Close();
		return serial;	
	}
}
